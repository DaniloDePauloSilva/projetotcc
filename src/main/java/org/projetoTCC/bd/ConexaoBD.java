package org.projetoTCC.bd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

public class ConexaoBD 
{
	private Connection conexao;
	private PreparedStatement stmtConsulta;
	private boolean autoCommit;
	private static GenericObjectPool objPool;
	private DataSource dataSource;
	
	public ConexaoBD() throws SQLException, ClassNotFoundException, Exception
	{
		this.dataSource = configurarPool();
		autoCommit = true;
	}
	
	public ConexaoBD(boolean autoCommit) throws SQLException, ClassNotFoundException, Exception
	{
		this.dataSource = configurarPool();
		this.autoCommit = autoCommit;
	}
	
	public DataSource configurarPool() throws Exception 
	{
		Class.forName("com.mysql.cj.jdbc.Driver");
		
		objPool = new GenericObjectPool();
		objPool.setMaxActive(1000);
	
		
		ConnectionFactory cf = new DriverManagerConnectionFactory(getStrConexao(), getUserName(), getPassword());
		PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf, objPool, null, null, false, true);
		
		return new PoolingDataSource(objPool);
	}

	private Connection conectar() throws SQLException
	{
		Connection con = dataSource.getConnection();
		con.setAutoCommit(autoCommit);
//		con.setNetworkTimeout(Executors.newFixedThreadPool(1), 60000);
		
		return con;
	}
	
	public void fecharConexao() throws SQLException
	{
		stmtConsulta.close();
		conexao.close();
		
		conexao = null;
	}
	
	private String getStrConexao() 
	{
//		return "jdbc:mysql://localhost:3306/projetoTCC?useTimezone=true&serverTimezone=UTC";
		return "jdbc:mysql://191.252.92.38:3306/projetoTCC?useTimezone=true&serverTimezone=UTC";
	}
	
	private String getUserName()
	{
//		return "root";
		return "projetoTCC";
	}
	
	private String getPassword()
	{
//		return "dno@admdb";
		return "Qwe-1357";
	}
	
	public void commit() throws SQLException
	{
		conexao.commit();
	}
	
	public void rollBack() throws SQLException
	{
		conexao.rollback();
	}
	
	private void atribuirValoresPS(PreparedStatement ps, List<Object> listaParametros) throws SQLException
	{
		int i = 1;
		for(int indice = 0; indice < listaParametros.size(); indice++)
		{
			Object valor = listaParametros.get(indice);
			
			if(valor instanceof Integer)
			{
				ps.setInt(i, (Integer) valor);
			}
			else if (valor instanceof Double)
			{
				ps.setDouble(i, (Double) valor);
			}
			else if(valor instanceof Long)
			{
				ps.setLong(i, (Long) valor);
			}
			else if(valor instanceof LocalDate)
			{
				ps.setString(i, ((LocalDate) valor).toString());
			}
			else if(valor instanceof LocalDateTime)
			{
				ps.setString(i, ((LocalDateTime) valor).toString());
			}
			else
			{
				// default -> String
				ps.setString(i, (String) valor);
			}
			
			i++;
		}
	}
	
	public void executaInsUpDel(String strSql, List<Object> listaParametros) throws SQLException
	{
		if(conexao == null)
			conexao = conectar();
		
		PreparedStatement ps = conexao.prepareStatement(strSql);
		
		System.out.println(listaParametros);
		
		if(listaParametros != null && listaParametros.size() > 0)
			atribuirValoresPS(ps, listaParametros); 

		System.out.println(ps);
		
		ps.execute();
		
		ps.close();
		
		if(autoCommit)
		{
			conexao.close();
		}
	}
	
	public void fecharStmtConsulta() throws Exception 
	{
		stmtConsulta.close();
	}
	
	public ResultSet retornaConsulta(String strSql, List<Object> listaParametros) throws SQLException
	{
		if(this.conexao == null)
			conexao = conectar();
		
		stmtConsulta = conexao.prepareStatement(strSql);
		
		if(listaParametros != null && listaParametros.size() > 0)
			atribuirValoresPS(stmtConsulta, listaParametros);
		
		ResultSet rs = stmtConsulta.executeQuery();
		
		return rs;
	}
	
	

}
