package org.projetoTCC.conf;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class SessionManager implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		HttpSession session = request.getSession();
		
		System.out.println("USUARIO SESSION: ");
		System.out.println(session.getAttribute("usuario"));
		
		if(session.getAttribute("usuario") == null)
		{
			System.out.println("NÃO AUTENTICADO");
			System.out.println("JSESSIONID=" + session.getId());
			
			response.sendRedirect(request.getContextPath() + "/nao-autenticado");
			
			return false;
		}
		else 
		{
			return true;
		}
		
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	
		
		
	}

}
