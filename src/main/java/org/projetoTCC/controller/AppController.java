package org.projetoTCC.controller;

import java.util.concurrent.Callable;

import org.projetoTCC.modelJson.RespostaJson;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {

	@RequestMapping(value="/nao-autenticado", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Callable<RespostaJson> naoAutenticado()
	{
		return () -> {
			
			RespostaJson resp = new RespostaJson();
			
			resp.setStatusResposta("FALHA_AUTENTICACAO");
			
			
			return resp;
			
		};
	}
	
	@RequestMapping(value="/nao-autenticado", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Callable<RespostaJson> naoAutenticadoGet()
	{
		return () -> {
			
			RespostaJson resp = new RespostaJson();
			
			resp.setStatusResposta("FALHA_AUTENTICACAO");
			
			
			return resp;
			
		};
	} 
	
}
