package org.projetoTCC.controller;

import javax.servlet.http.HttpSession;

import org.projetoTCC.modelJson.Teste;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value="/teste")
@RestController
public class TesteControllerRest {

	@RequestMapping(value="/teste", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Teste teste(HttpSession session) 
	{
		System.out.println("JSESSIONID=" + session.getId());
		
		Teste teste = new Teste();
		
		teste.setCodigoTeste(1234);
		teste.setDescricaoTeste("TESTE");
		
		return  teste;
	}
	
	@RequestMapping(value="/teste", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Teste testePost(HttpSession session) 
	{
		System.out.println("JSESSIONID=" + session.getId());
		
		Teste teste = new Teste();
		
		teste.setCodigoTeste(1234);
		teste.setDescricaoTeste("TESTE");
		
		return  teste;
	}
	
}
