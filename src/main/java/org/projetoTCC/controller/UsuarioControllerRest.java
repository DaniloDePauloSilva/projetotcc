package org.projetoTCC.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.servlet.http.HttpSession;

import org.projetoTCC.dao.UsuarioDao;
import org.projetoTCC.dao.VeiculosDao;
import org.projetoTCC.modelJson.DadosRetornoAutenticacaoUsuario;
import org.projetoTCC.modelJson.RespostaJson;
import org.projetoTCC.modelJson.UsuarioAutenticacaoJson;
import org.projetoTCC.modelJson.UsuarioJson;
import org.projetoTCC.modelJson.VeiculoJson;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuario")
public class UsuarioControllerRest {
	
	@RequestMapping(value="/autenticar", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Callable<RespostaJson> autenticarUsuario(@RequestBody String strJson, HttpSession session)
	{
		return () -> {
			
			RespostaJson resp = new RespostaJson();
			
			try 
			{
				System.out.println("RECEBEU POST AUTENTICACAO: ");
				System.out.println(strJson);
				
				UsuarioAutenticacaoJson usrAut = UsuarioAutenticacaoJson.fromJson(strJson);
				
				UsuarioDao usuarioDao = new UsuarioDao();
				UsuarioJson usr = usuarioDao.autenticarUsuario(usrAut.getEmail(), usrAut.getSenha());
			
				if(usr != null)
				{
					System.out.println("USUÁRIO AUTENTICADO");
					
//					
//					String hashSessao = usuarioDao.registrarSessao(usr);
					
					DadosRetornoAutenticacaoUsuario retorno = new DadosRetornoAutenticacaoUsuario();
					
					session.setAttribute("usuario", usr);
					
					retorno.setUsuario(usr);
					retorno.setJSESSIONID(session.getId());
					
					System.out.println("JSESSIONID=" + session.getId());
					
					resp.setStatusResposta("OK");
					resp.setRetorno(retorno);
				}
				else 
				{
					resp.setStatusResposta("FALHA_AUTENTICACAO");
					resp.setRetorno(null);
				}
				
				return resp;
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				resp.setStatusResposta("ERRO");
				resp.setRetorno("ERRO: " + ex.getMessage());
				return resp;
			}
		};
	}
	
	@RequestMapping(value="/veiculos-usuario", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Callable<List<VeiculoJson>> retornaVeiculosDoUsuario(@RequestParam Map<String, String> param)
	{
		return () -> 
		{
			int codigoUsuario = 0;
			
			if(param.containsKey("codigo-usuario"))
			{
				try 
				{
					codigoUsuario = Integer.parseInt(param.get("codigo-usuario"));
				}
				catch(Exception ex)
				{
					return new ArrayList<>();
				}
			}
			
			if(codigoUsuario == 0)
			{
				return new ArrayList<>();
			}
			
			List<VeiculoJson> listaVeiculos = new VeiculosDao().retornaListaVeiculosDoUsuario(codigoUsuario);
			
			
			return listaVeiculos;
		};
	}

}
