package org.projetoTCC.controller;

import java.util.Map;
import java.util.concurrent.Callable;

import javax.servlet.http.HttpSession;

import org.projetoTCC.dao.VeiculosDao;
import org.projetoTCC.exception.ExceptionVeiculoNaoEncontrado;
import org.projetoTCC.modelJson.MensagemLocalizacaoVeiculo;
import org.projetoTCC.modelJson.RespostaJson;
import org.projetoTCC.modelJson.UsuarioJson;
import org.projetoTCC.modelJson.VeiculoJson;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/veiculos")
public class VeiculosController {
	
	@RequestMapping(value="/gravar-localizacao-veiculo", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public Callable<RespostaJson> gravarLocalizacaoVeiculo(@RequestBody String json)
	{
		return () ->{
			
			RespostaJson resp = new RespostaJson();
			
			try 
			{
				System.out.println("GRAVAR LOCALIZACAO VEICULO - RECEBEU MENSAGEM: ");
				System.out.println(json);
				
				ObjectMapper om = new ObjectMapper();
				MensagemLocalizacaoVeiculo msg = om.readValue(json, MensagemLocalizacaoVeiculo.class);
				
				new VeiculosDao().gravarLocalizacaoVeiculo(msg);
				
				resp.setStatusResposta("OK");
				
				return resp;
			}
			catch(Exception ex)
			{
				resp.setStatusResposta("ERRO: " + ex.getMessage());
				
				return resp;
			}
			
		};
	}
	
	@RequestMapping(value="/retorna-veiculo", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public Callable<RespostaJson> retornaVeiculo(@RequestParam Map<String, String> param, HttpSession session)
	{
		return () -> {
			RespostaJson resp = new RespostaJson();
			
			try 
			{
				int codigoVeiculo = Integer.parseInt(param.get("codigo-veiculo"));
				
				UsuarioJson usr = (UsuarioJson) session.getAttribute("usuario");
				VeiculoJson veiculo = new VeiculosDao().retornaVeiculo(usr.getCodigoUsuario(), codigoVeiculo);
				
				
				resp.setStatusResposta("OK");
				resp.setRetorno(veiculo);
				
				return resp;
			}
			catch(ExceptionVeiculoNaoEncontrado ex)
			{
				resp.setStatusResposta("ERRO");
				resp.setRetorno("VEICULO NÃO ENCONTRADO");
				
				return resp;
			}
			catch(Exception ex)
			{
				resp.setStatusResposta("ERRO");
				
				return resp;
			}
		};
	}
	
	@RequestMapping(value="/retorna-veiculos-usuario", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public Callable<RespostaJson> retornaListaVeiculo1sDoUsuario(@RequestParam Map<String, String> param, HttpSession session)
	{
		return () -> {
			
			RespostaJson resp = new RespostaJson();
			
			try 
			{
				UsuarioJson usr = (UsuarioJson) session.getAttribute("usuario");
				
				System.out.println("CÓDIGO DO USUARIO: " + usr.getCodigoUsuario());
				
				resp.setRetorno(new VeiculosDao().retornaListaVeiculosDoUsuario(usr.getCodigoUsuario()));
				
				resp.setStatusResposta("OK");
				
				return resp;
			}
			catch(Exception ex) 
			{
				ex.printStackTrace();
				resp.setStatusResposta("ERRO");
				
				return resp;
			}
		};
		
	}
	

}
