package org.projetoTCC.dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.projetoTCC.bd.ConexaoBD;
import org.projetoTCC.exception.ExceptionVeiculoNaoEncontrado;
import org.projetoTCC.modelJson.MensagemLocalizacaoVeiculo;
import org.projetoTCC.modelJson.UsuarioJson;
import org.projetoTCC.modelJson.VeiculoJson;

public class VeiculosDao {
	
	
	public void gravarLocalizacaoVeiculo(MensagemLocalizacaoVeiculo msg) throws Exception 
	{
		ConexaoBD con = new ConexaoBD();
		List<Object> listaParametros = new ArrayList<>();
		
		StringBuilder strSql = new StringBuilder();
		
		strSql.append("insert into localizacao_veiculo (codigo_veiculo, lat, lng, data_registro) ");
		strSql.append("values (?, ?, ?, now())");
		
		listaParametros.add(msg.getCodigoVeiculo());
		listaParametros.add(msg.getLatitude());
		listaParametros.add(msg.getLongitude());
		
		con.executaInsUpDel(strSql.toString(), listaParametros);
		
	}
	
	
	public void carregarVeiculosDoUsuario(UsuarioJson usuario) throws Exception
	{
		ConexaoBD con = new ConexaoBD();
		List<Object> listaParametros = new ArrayList<>();
		
		StringBuilder strSql = new StringBuilder();
		
		strSql.append("select v.codigo_veiculo, v.descricao as descricao_veiculo, v.placa, ");
		strSql.append("l.codigo_localizacao, l.lat, l.lng, l.data_registro ");
		strSql.append("from veiculos v ");
		strSql.append("join localizacao_veiculo l on l.codigo_veiculo = v.codigo_veiculo ");
		strSql.append("join usuarios u on u.codigo_usuario = v.codigo_usuario ");
		strSql.append("where u.codigo_usuario = ? ");
		strSql.append("and l.codigo_localizacao = (select max(codigo_localizacao) from localizacao_veiculo where codigo_veiculo = v.codigo_veiculo)");
		
		listaParametros.add(usuario.getCodigoUsuario());
		
		ResultSet rs = con.retornaConsulta(strSql.toString(), listaParametros);
		
		while(rs.next()) 
		{
			VeiculoJson veiculo = new VeiculoJson();
			
			veiculo.setCodigoVeiculo(rs.getInt("codigo_veiculo"));
			veiculo.setDescricao(rs.getString("descricao_veiculo"));
			
			
			usuario.getListaVeiculos().add(veiculo);	
		}
		
		con.fecharConexao();
	}
	
	public List<VeiculoJson> retornaListaVeiculosDoUsuario(int codigoUsuario) throws Exception
	{
		ConexaoBD con = new ConexaoBD();
		List<VeiculoJson> listaRetorno = new ArrayList<>();
		List<Object> listaParametros = new ArrayList<>();
		
		String strSql = strSqlVeiculos();
		
		listaParametros.add(codigoUsuario);
		
		ResultSet rs = con.retornaConsulta(strSql, listaParametros);
		
		while(rs.next())
		{
			VeiculoJson v = new VeiculoJson();
			
			v.setCodigoVeiculo(rs.getInt("codigo_veiculo"));
			v.setDescricao(rs.getString("descricao_veiculo"));
			v.setPlaca(rs.getString("placa"));
			v.getUltimaLocalizacao().setCodigoLocalizacao(rs.getInt("codigo_localizacao"));
			v.getUltimaLocalizacao().setLat(rs.getBigDecimal("lat"));
			v.getUltimaLocalizacao().setLng(rs.getBigDecimal("lng"));
			v.getUltimaLocalizacao().setDataRegistro(rs.getString("data_registro"));
			
			listaRetorno.add(v);
		}
		
		return listaRetorno;
		
	}
	
	public String strSqlVeiculos() 
	{
		StringBuilder strSql = new StringBuilder();
		
		strSql.append("select v.codigo_veiculo, v.descricao as descricao_veiculo, v.placa, ");
		strSql.append("l.codigo_localizacao, l.lat, l.lng, l.data_registro ");
		strSql.append("from veiculos v ");
		strSql.append("left join localizacao_veiculo l on l.codigo_veiculo = v.codigo_veiculo ");
		strSql.append("join usuarios u on u.codigo_usuario = v.codigo_usuario ");
		strSql.append("where u.codigo_usuario = ? ");
		strSql.append("and l.codigo_localizacao = (select max(codigo_localizacao) from localizacao_veiculo where codigo_veiculo = v.codigo_veiculo)");
		
		return strSql.toString();
	}

	public VeiculoJson retornaVeiculo(int codigoUsuario, int codigoVeiculo) throws Exception
	{
		ConexaoBD con = new ConexaoBD();
		List<Object> listaParametros = new ArrayList<>();
		
		String strSql = strSqlVeiculos();
		
		ResultSet rs = con.retornaConsulta(strSql.toString(), listaParametros);
		
		if(rs.next())
		{
			VeiculoJson v = new VeiculoJson();
			
			v.setCodigoVeiculo(rs.getInt("codigo_veiculo"));
			v.setDescricao(rs.getString("descricao_veiculo"));
			v.setPlaca(rs.getString("placa"));
			v.getUltimaLocalizacao().setCodigoLocalizacao(rs.getInt("codigo_localizacao"));
			v.getUltimaLocalizacao().setLat(rs.getBigDecimal("lat"));
			v.getUltimaLocalizacao().setLng(rs.getBigDecimal("lng"));
			v.getUltimaLocalizacao().setDataRegistro(rs.getString("data_registro"));
			
			return v;
		}
		else 	
		{
			throw new ExceptionVeiculoNaoEncontrado("VEÍCULO NÃO ENCONTRADO"); 
		}
		
	}

}
