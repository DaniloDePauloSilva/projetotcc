package org.projetoTCC.exception;

public class ExceptionVeiculoNaoEncontrado extends Exception {
	
	public ExceptionVeiculoNaoEncontrado() 
	{
		super();
	}
	
	public ExceptionVeiculoNaoEncontrado(String mens)
	{
		super(mens);
	}

}
