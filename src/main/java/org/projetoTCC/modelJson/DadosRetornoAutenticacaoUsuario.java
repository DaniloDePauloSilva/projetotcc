package org.projetoTCC.modelJson;

import java.util.List;

public class DadosRetornoAutenticacaoUsuario {
	
	private UsuarioJson usuario;
	private String hashSessao;
	private String JSESSIONID;
	
	public UsuarioJson getUsuario() {
		return usuario;
	}
	
	public void setUsuario(UsuarioJson usuario) {
		this.usuario = usuario;
	}
	
	public String getHashSessao() {
		return hashSessao;
	}
	
	public void setHashSessao(String hashSessao) {
		this.hashSessao = hashSessao;
	}

	public String getJSESSIONID() {
		return JSESSIONID;
	}

	public void setJSESSIONID(String jSESSIONID) {
		JSESSIONID = jSESSIONID;
	}
}
