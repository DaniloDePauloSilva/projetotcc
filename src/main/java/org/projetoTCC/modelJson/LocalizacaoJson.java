package org.projetoTCC.modelJson;

import java.math.BigDecimal;

public class LocalizacaoJson {
	
	private Integer codigoLocalizacao;
	private BigDecimal lat;
	private BigDecimal lng;
	private String dataRegistro;
	
	public Integer getCodigoLocalizacao() {
		return codigoLocalizacao;
	}
	public void setCodigoLocalizacao(Integer codigoLocalizacao) {
		this.codigoLocalizacao = codigoLocalizacao;
	}
	public BigDecimal getLat() {
		return lat;
	}
	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}
	public BigDecimal getLng() {
		return lng;
	}
	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}
	public String getDataRegistro() {
		return dataRegistro;
	}
	public void setDataRegistro(String dataRegistro) {
		this.dataRegistro = dataRegistro;
	}
	

}
