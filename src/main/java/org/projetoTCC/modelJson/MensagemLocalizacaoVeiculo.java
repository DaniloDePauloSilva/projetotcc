package org.projetoTCC.modelJson;

public class MensagemLocalizacaoVeiculo {
	
	private int codigoVeiculo;
	private Double latitude;
	private Double longitude;
	
	public int getCodigoVeiculo() {
		return codigoVeiculo;
	}
	
	public void setCodigoVeiculo(int codigoVeiculo) {
		this.codigoVeiculo = codigoVeiculo;
	}
	
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

}
