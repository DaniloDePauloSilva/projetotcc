package org.projetoTCC.modelJson;

public class Teste {
	
	private Integer codigoTeste;
	private String descricaoTeste;
	
	public Integer getCodigoTeste() 
	{
		return codigoTeste;
	}
	
	public void setCodigoTeste(Integer codigoTeste) 
	{
		this.codigoTeste = codigoTeste;
	}
	
	public String getDescricaoTeste() 
	{
		return descricaoTeste;
	}
	
	public void setDescricaoTeste(String descricaoTeste) 
	{
		this.descricaoTeste = descricaoTeste;
	}
	
	

}
