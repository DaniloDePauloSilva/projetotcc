package org.projetoTCC.modelJson;

import com.fasterxml.jackson.databind.ObjectMapper;

public class UsuarioAutenticacaoJson {
	
	private String email;
	private String senha;
	
	public String getEmail() 
	{
		return email;
	}
	
	public void setEmail(String email) 
	{
		this.email = email;
	}
	
	public String getSenha() 
	{
		return senha;
	}
	
	public void setSenha(String senha) 
	{
		this.senha = senha;
	}
	
	public static UsuarioAutenticacaoJson fromJson(String strJson) throws Exception 
	{
		ObjectMapper om = new ObjectMapper();
		
		UsuarioAutenticacaoJson usuario = om.readValue(strJson, UsuarioAutenticacaoJson.class);
		
		
		return usuario;
	}
}
