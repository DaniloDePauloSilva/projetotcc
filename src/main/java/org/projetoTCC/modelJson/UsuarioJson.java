package org.projetoTCC.modelJson;

import java.util.ArrayList;
import java.util.List;

import org.projetoTCC.dao.UsuarioDao;
import org.projetoTCC.dao.VeiculosDao;

public class UsuarioJson {

	private int codigoUsuario;
	private String nome;
	private String email;
	private String senha;
	private List<VeiculoJson> listaVeiculos;
	
	
	public int getCodigoUsuario() {
		return codigoUsuario;
	}
	public void setCodigoUsuario(int codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public List<VeiculoJson> getListaVeiculos() {
		
		if(this.listaVeiculos == null)
			this.listaVeiculos = new ArrayList<>();
		
		return listaVeiculos;
	}
	public void setListaVeiculos(List<VeiculoJson> listaVeiculos) {
		this.listaVeiculos = listaVeiculos;
	}
	
	
	public boolean autenticar(String usuario, String senha) throws Exception 
	{
		UsuarioJson usr = new UsuarioDao().autenticarUsuario(usuario, senha);
		
		if(usr != null)
		{
			this.setCodigoUsuario(usr.getCodigoUsuario());
			this.setNome(usr.getNome());
			this.setEmail(usr.getEmail());
		
			new VeiculosDao().carregarVeiculosDoUsuario(this);
			
			return true;
		}
		else 
		{
			return false;
		}
	}
}
