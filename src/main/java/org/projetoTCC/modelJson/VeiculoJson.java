package org.projetoTCC.modelJson;

public class VeiculoJson {
	
	private Integer codigoVeiculo;
	private String descricao;
	private String placa;
	private LocalizacaoJson ultimaLocalizacao;
	
	public Integer getCodigoVeiculo() 
	{
		return codigoVeiculo;
	}
	
	public void setCodigoVeiculo(Integer codigoVeiculo) 
	{
		this.codigoVeiculo = codigoVeiculo;
	}
	
	public String getDescricao() 
	{
		return descricao;
	}
	
	public void setDescricao(String descricao) 
	{
		this.descricao = descricao;
	}

	public void setPlaca(String placa) {
		
		this.placa = placa;
	}
	
	public String getPlaca() 
	{
		return this.placa;
	}

	public LocalizacaoJson getUltimaLocalizacao() {
		
		if(ultimaLocalizacao == null)
			ultimaLocalizacao = new LocalizacaoJson();
		
		return ultimaLocalizacao;
	}

	public void setUltimaLocalizacao(LocalizacaoJson ultimaLocalizacao) {
		this.ultimaLocalizacao = ultimaLocalizacao;
	}
}
